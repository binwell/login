# Binwell Login for Xamarin.Forms 

Demo of using native SDK for social networks login.

Here is an articles (in Russian for now) about using it:

[Xamarin.Forms and native Facebok SDK](https://medium.com/binwell/3b3c0e783a62)

[Xamarin.Forms and native VK SDK](https://medium.com/binwell/46989e420f7)

[Xamarin.Forms and OAuth](https://medium.com/binwell/3edada9ed50e)